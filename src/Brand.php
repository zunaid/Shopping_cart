<?php
/**
 * Created by PhpStorm.
 * User: Zunaid
 * Date: 10/17/2017
 * Time: 11:57 AM
 */

namespace App;
use App\Database as DB;
use App\Utility;
use App\Message;
use PDO;

class Brand extends DB
{
    private $id;
    private $brandName;

   public function setData($allPostData=NULL){

       if(array_key_exists('id',$allPostData)){

           $this->id = $_POST['id'];

       }

       if(array_key_exists('brand_name',$allPostData)){

           $this->brandName = $_POST['brand_name'];
       }
   }

    public function store(){

        $arrData = array($this->brandName);

        $sql= "INSERT INTO brand(brandName) VALUE (?)";

        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($arrData);

        if($result){

            Message::setMessage("Success! Data has been stored successfully.");
        }else{
            Message::setMessage("Failed! Data has not been stored");
        }
        utility::redirect('brandlist.php');

    }

    public function select(){

        $sql ="SELECT *FROM brand ORDER BY brandID";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();
    }

    public function view(){
        $sql= "SELECT * FROM  brand WHERE brandID=''" . $this->id;
        $STH = $this->DBH->query($sql);
        $STH->fetch(PDO::FETCH_ASSOC);
        return $STH->fetch();
    }

}