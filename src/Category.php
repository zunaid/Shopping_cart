<?php


namespace App;

use App\Database as DB;

use App\Message;

use App\Utility;
use PDO;

class Category extends DB
{
    private $category_ID;

    private $category_Name;

    public function setData($allPostData = null)
    {
        if(array_key_exists('category_ID',$allPostData)){

            $this->category_ID = $_POST['category_ID'];
        }

        if (array_key_exists('category_name', $allPostData)) {

            $this->category_Name = $_POST['categoryName'];
        }

    }

    public function store()
    {

        $arrData = array($this->category_Name);

        $query = "INSERT into category(categoryName) VALUES (?)";

        $STH = $this->DBH->prepare($query);

        $result = $STH->execute($arrData);

        if ($result) {

            Message::setMessage("Success! Data has been stored successfully");
        } else {
            Message::setMessage("Failed! Data has not been stored ");
        }
        utility::redirect('categoryList.php');
    }

    public function view()
    {

        $sql = "SELECT * FROM category WHERE categoryID=''" . $this->category_ID;
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }


    public function select()
    {

        $sql = 'SELECT * FROM category ORDER BY categoryID';

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();
    }

    public function delete(){

        $sql= "DELETE FROM  category WHERE categoryID=''".$this->category_ID;
        $result = $this->DBH->exec($sql);
        if($result){
            Message::setMessage("Success! Data has been deleted");
        }
        else {
            Message::setMessage("Failed! Data has not been deleted ");
        }

        utility::redirect('categoryList.php');

    }


}