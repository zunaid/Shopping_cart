<?php
  require_once ("vendor/autoload.php");
   $product = new App\Product();
   $product->setData($_GET);
   $allData =$product->getCart();
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Online Shop</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="resource/css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
    table {
    border-collapse: separate;
    border-spacing: 0;
    }
    </style>

</head>
<body>
<?php include_once("navbar.php");?>
<div class="container" style="height: 500px">
    <div class="row">
         <div class="col-md-8 col-sm-8">
                <div class="panel panel-default">
                    <div class="panel-heading">Product Details</div>
                    <div class="panel-body">
                            <table class="table table-striped">
                                <tbody class="table">
                                <tr class="row-1 odd">
                                    <td class="column-1"><img src="http://www.mobiledokan.com/wp-content/uploads/2018/02/Xiaomi-Redmi-5.jpg" alt="Xiaomi Redmi 5" width="148" height="192"></td><td class="column-2"><h2><span style="color: #ff0000;"><strong><span style="color: #808080;">Price in Bangladesh:</span> <span style="color: #008000;">13,990 Tk.</span></strong></span></h2><br>
                                        <a class=" button-st button-with-icon button-with-icon-16 ico-st ico-cart" href="http://www.mobiledokan.com/xiaomi-stores-bangladesh/" target="_parent" style=""><span>Store</span></a></td>
                                </tr>
                                <tr class="row-2 even">
                                    <td class="column-1">Network Scope</td><td class="column-2">2G, 3G, 4G</td>
                                </tr>
                                <tr class="row-3 odd">
                                    <td class="column-1">Battery Type &amp; Performance</td><td class="column-2">Lithium-polymer 3300 mAh (non-removable)</td>
                                </tr>
                                <tr class="row-4 even">
                                    <td class="column-1">Body &amp; Weight</td><td class="column-2">151.8 x 72.8 x 7.7 millimeter, 157 grams (plastic frame &amp; ends, aluminum back)</td>
                                </tr>
                                <tr class="row-5 odd">
                                    <td class="column-1">Camera Factors (Back)</td><td class="column-2">f/2.2, 1.25 μm, phase detection autofocus, LED flash, auto face &amp; smile detection, HDR, panorama mode</td>
                                </tr>
                                <tr class="row-6 even">
                                    <td class="column-1">Camera Resolution (Back)</td><td class="column-2">12 Megapixel</td>
                                </tr>
                                <tr class="row-7 odd">
                                    <td class="column-1">Camera Resolution (Front)</td><td class="column-2">5 Megapixel (selfie light, Full HD video rec.)</td>
                                </tr>
                                <tr class="row-8 even">
                                    <td class="column-1">Chipset</td><td class="column-2">Qualcomm Snapdragon 450</td>
                                </tr>
                                <tr class="row-9 odd">
                                    <td class="column-1">Colors Available</td><td class="column-2">Black, Gold, Light Blue, Rose Gold</td>
                                </tr>
                                <tr class="row-10 even">
                                    <td class="column-1">Display Size &amp; Resolution</td><td class="column-2">5.7 inches (18:9 full screen display), HD+ 720 x 1440 pixels (282 ppi)</td>
                                </tr>
                                <tr class="row-11 odd">
                                    <td class="column-1">Display Type</td><td class="column-2">IPS LCD Touchscreen with Corning Gorilla Glass (unspecified version) protection</td>
                                </tr>
                                <tr class="row-12 even">
                                    <td class="column-1">Graphics Processing Unite (GPU)</td><td class="column-2">Adreno 506</td>
                                </tr>
                                <tr class="row-13 odd">
                                    <td class="column-1">Memory Card Slot</td><td class="column-2">MicroSD, up to 128 GB (uses SIM 2 slot)</td>
                                </tr>
                                <tr class="row-14 even">
                                    <td class="column-1">Operating System</td><td class="column-2">Android Nougat v7.1.2 (based on MIUI 9.1)</td>
                                </tr>
                                <tr class="row-15 odd">
                                    <td class="column-1">Processor</td><td class="column-2">Octa-core,1.8 GHz</td>
                                </tr>
                                <tr class="row-16 even">
                                    <td class="column-1">RAM</td><td class="column-2">2 GB</td>
                                </tr>
                                <tr class="row-17 odd">
                                    <td class="column-1">ROM</td><td class="column-2">16 GB</td>
                                </tr>
                                <tr class="row-18 even">
                                    <td class="column-1">Release Date</td><td class="column-2">December 2017</td>
                                </tr>
                                <tr class="row-19 odd">
                                    <td class="column-1">Sensors</td><td class="column-2">Fingerprint, accelerometer, gyro, proximity, compass</td>
                                </tr>
                                <tr class="row-20 even">
                                    <td class="column-1">SIM Card Type</td><td class="column-2">Hybrid Dual SIM (Nano-SIM, dual standby, 4G support in both slots)</td>
                                </tr>
                                <tr class="row-21 odd">
                                    <td class="column-1">USB</td><td class="column-2">MicroUSB v2.0</td>
                                </tr>
                                <tr class="row-22 even">
                                    <td class="column-1">Video Recording</td><td class="column-2">Full HD (1080p)</td>
                                </tr>
                                <tr class="row-23 odd">
                                    <td class="column-1">Wireless LAN</td><td class="column-2">Yes, Wi-Fi Direct, hotspot</td>
                                </tr>
                                <tr class="row-24 even">
                                    <td class="column-1">Other Features</td><td class="column-2">- Bluetooth, GPS, A-GPS, MP3, MP4, Radio, GPRS, Edge, Loudspeaker, Multitouch</td>
                                </tr>
                                </tbody>
                            </table>
                    </div>
                </div>
            </div>
        <?php


        ?>
        <div class="col-md-4 col-sm-4">
            <?php
                foreach($allData as $oneData){
            ?>
        <img src="resource/image/<?php echo $oneData->image;?>" class="img-responsive" height="200" width="150" alt="">

        <h3><?php echo $oneData->productName; ?></h3>
        <label for="category">Category : <?php echo $oneData->categoryName; ?></label><br><br>
        <label for="brand">Brand : <?php echo $oneData->brandName; ?></label><br><br>
        <label for="brand">Price : <?php echo $oneData->price; ?> Tk.</label><br><br>
            <form action="addCart.php" method="post">
                <label for="">Quantity :
                    <input type="number" min="1" max="10" value="1" step="1"  style="width: 40px">
                </label><br>
            </form>
        <input type="submit" class="btn btn-success pull-right" value="ADD TO CART">
    </div>
        <?php  } ?>
    </div>
</div>

<div class="container">

                <?php include_once("footer.php");?>

    </div>

</div>




<script src="resource/js/jquery-1.11.1.min.js"></script>
<script src="resource/js/bootstrap.min.js"></script>
</body>
</html>