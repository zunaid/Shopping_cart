<?php
require_once ("../../vendor/autoload.php");



?>



<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Brand</title>
    <link href="../../resource/style.css" rel="stylesheet">
    <link href="../../resource/bootstrap.min.css" rel="stylesheet">
    <style>
        #category{
            padding-top: 50px;
        }
    </style>
</head>
<body>
<div class="container-fluid" id="category" align="center">
    <form action="store.php" method="post">
        <p>Enter Brand Name:
            <input type="text" class="form-group-sm" name="brand_name">&nbsp;
            <input type="submit" class="btn btn-primary" value="Enter" align="center">
        </p>
    </form>
</div>
<script src="../../resource/jquery.min.js"></script>


</body>
</html>