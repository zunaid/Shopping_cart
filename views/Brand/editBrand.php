<?php
require_once ('../../vendor/autoload.php');

$brand = new App\Brand();

$oneData = $brand->view();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Editing Brand Name</title>
    <link href="../../resource/style.css" rel="stylesheet">
    <link href="../../resource/bootstrap.min.css" rel="stylesheet">
    <style>
        .container{
            padding: 100px 10px 10px 10px;
        }
    </style>
</head>
<body>
<div class="container" align="center">
    <form action="store.php"  method="post">
        <table>
        <tr>
            <td>Brand Name:</td>
            <td>
                <input type="text" class="form-group-sm" name="brand_name" value="<?php echo $oneData->brandName;?>">
            </td>
         </tr>
        </table>
    </form>
</div>


</body>
</html>