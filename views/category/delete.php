<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Delete Category</title>
    <link href="../../resource/style.css" rel="stylesheet">
    <link href="../../resource/bootstrap.min.css" rel="stylesheet">
</head>



<body>
<div class="container">


    <?php
    require_once ("../../vendor/autoload.php");

    use App\Category;

    use App\Database;

    $category = new Category();

    $category->setData($_GET);

    $id=$_GET['id'];


    echo "<p>Are you sure want to delete this category</p>
     <a href='delete.php?Yes=1&id=$id' class='btn btn-sm btn-danger'>Yes</a>
    <a href='categoryList.php' class='btn btn-sm btn-info'>No</a>";


    if(isset($_GET['Yes'])){
        $category->setData($_GET);
        $oneData = $category->delete();
    }
    ?>


    
</div>
  

</body>
</html>
