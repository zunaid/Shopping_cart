<?php
require_once ('../../vendor/autoload.php');

use App\Category;
use App\Database;
use App\Message;

$category = new Category();

$allData=$category->select();

if(!isset($_SESSION)){
    session_start();
}

$msg = Message::getMessage();

echo "<div style='height: 30px'> <div   id='message'> $msg </div> </div>";






?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Category List</title>
    <link href="../../resource/style.css" rel="stylesheet">
    <link href="../../resource/bootstrap.min.css" rel="stylesheet">
</head>
<body>

<div class="container" align="center">
    <h2 align="center">Category List</h2>
    <table class="table table-striped table-bordered table-hover">
        <tr>
            <th>Serial No.</th>
            <th>Category Name</th>
            <th>Action </th>
        </tr>
        <?php
                  $serial = 1;
                  foreach($allData as $oneData) {

                      echo "
                       <tr>
                           <td>$serial</td>
                           <td>$oneData->categoryName</td>
                           <td>
                               <a href='editCategory.php?id=$oneData->categoryID' class='btn btn-sm btn-info'>Edit</a>
                               <a href='delete.php?id=$oneData->categoryID' class='btn btn-sm btn-danger'>Delete</a>
                           </td>
                       </tr>
                  ";
                      $serial++;
                  }



        ?>

    </table>

</div>



<script src="../../resource/jquery.min.js"></script>
<script src="../../resource/jquery-1.11.1.min.js"></script>

<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>



</body>
</html>
